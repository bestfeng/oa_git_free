<div align="center">
<img alt="Static Badge" src="https://img.shields.io/badge/build-Apache2.0-brightgreen?style=flat&label=License&cacheSeconds=3600&link=https%3A%2F%2Fgitee.com%2Fbestfeng%2Foa_git_free">
<img alt="Static Badge" src="https://img.shields.io/badge/build-%E4%BA%91%E7%BD%91%E8%BD%AF%E4%BB%B6-rgb(248%2C106%2C7)?style=flat&label=Author&cacheSeconds=3600&link=https%3A%2F%2Fgitee.com%2Fbestfeng%2Foa_git_free">
<img alt="Static Badge" src="https://img.shields.io/badge/build-8.0-blue?style=flat&label=Release&cacheSeconds=3600&link=https%3A%2F%2Fgitee.com%2Fbestfeng%2Foa_git_free">
<img alt="Static Badge" src="https://img.shields.io/badge/build-Vue3.0-brightgreen?style=flat&label=Support&cacheSeconds=3600&link=https%3A%2F%2Fgitee.com%2Fbestfeng%2Foa_git_free">
</div>

## <img alt="logo" src="https://images.gitee.com/uploads/images/2021/1012/212212_cb5634a3_48408.png" width="28" height="28" style="vertical-align:middle"  width="42" height="42"/>    行云流程引擎，16年打造，流程专家

### 介绍

- 行云流程引擎（TFlow）源自云网OA，自2006年起，历经16年，服务过上千家客户。<br>
- 众多客户的打磨，众多需求的驱求，已使其成为一个成熟高效的流程引擎。<br>
- 设计轻巧灵活，功能丰富成熟，上手超简单。<br>
- 如果您需要低代码平台，请点击这里 [云网OA低代码开发平台](https://gitee.com/bestfeng/yimioa '云网OA')<br>

### 我们的目标<br>
- 打造业内领先的超级流程引擎。<br>
	
- 为方便小伙伴们学习使用，进群可享有免费培训 <br>
- 页面底部有群二维码，欢迎加入！

### 流程及表单引擎开源

🏅 流程引擎100%开源 <br>
🏅 表单引擎100%开源 <br>

### 技术栈

- 基于JAVA开发，支持MySQL/Oracle/SQLServer数据库<br>
- 后端框架：spring boot + mybatis plus + redis + Druid + ActiveMQ/RocketMQ<br>
- 前端框架：Vue3 + Ant Design + Vben Admin(优秀前端框架)<br>

### 功能比较
详见：[开源流程引擎对比完整版](https://docs.qq.com/sheet/DQktkbmtFc2lUbU9N?tab=BB08J2)<br>
<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/TFlow_compare.png" width=800 ><br>

### 流程引擎
- 作为OA的核心，工作流引擎功能强大，整体功能类似Activiti，但操作更方便  <br>
- 集成在线脚本设计器，可在线编写java代码，响应流程事件的处理，很简单就能打通模块和第三方应用<br>
- 首家独创一站式流程调试，无论流程中多少个节点，只需用一个帐户登录就可以测完全过程，实施效率提升200%<br>

<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/desktop.png" width=800 ><br>

<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/flow_launch.png" width=800 ><br>

### 后台管理功能

- 具有完整的后台管理功能，包括：组织管理、用户管理、角色管理、用户组管理、岗位管理、权限管理、消息、通知公告等<br>
- 支持RBAC3权限模型，权限可赋予给角色、组织、岗位、用户组、用户<br>
- 支持菜单管理，可以方便地增加自定义功能<br>
- 支持手机APP、小程序，集成钉钉、企业微信接口<br>

### 流程功能

| 功能     | 描述                                                 |
|--------|----------------------------------------------------|
|流程图    | 可视化流程设计，一体化流程编辑环境|
|多起点    | 多个起点自动适配发起人，省去大量的条件判断，极大降低流程复杂度|
|流程路由  | 支持顺序流、并发流、自由流；支持会签、撤销、分发等|
|选择策略  | 支持竞争、最闲者优先、角色中X人处理等策略|
|超时策略  | 超时可等待、自动交办至下一节点或返回给发起人|
|支持加签   |可临时加入用户审批，支持前加签、后加签|
|支持跳签   |跳签可在没有匹配到用户的情况下跳过节点，是支持复杂组织机构的利器|
|二维协同   |支持在流程中回复，边讨论边确定审批结果，并留下痕迹|
|条件分支   |角色、人员、表单数据可灵活组合，并可通过脚本进行条件判断|
|流程提醒   |支持消息、短信、邮件方式提醒流程事项|
|异步提交   |节点上有多个人员同时处理时，可以分别提交，适用于任务下达的情况|

### 表单功能

| 功能     | 描述                                                 |
|--------|----------------------------------------------------|
|控件    | 除了基本控件类型，还有富文本、图片、文件等|
|嵌套表格    | 可在主表中插入多行记录，并支持分页、查询、拉单（按条件手动或自动拉取别的表中的记录）|
|生成表格  | 支持在数据库中生成对应表格及相应类型的字段|
|有效性验证  | 客户端和服务器端支持有效性验证|
|自动冲抵   |如：报销流程走完后，自动冲抵所拉取的借款记录，将来便不会再被拉取到|
|选择带入   |支持选择记录带入相关字段，如选择项目，带入项目地址、完成时间、可用预算等|
|支持拉单   |可选择多条记录一起拉取，并可以自动拉单，如：选择人员，自动拉取其借款记|
|显示规则   |支持根据条件判断显示或隐藏区域|
|验证规则   |支持根据条件判断进行表单数据有效性校验|
|宏控件  | 共61个宏控件，包括：部门选择、人员选择、角色选择、手写签名、意见框等|
|宏控件标准   |宏控件形成了开发标准规范，只需两个文件就能实现|

### 高级BPM特性

🎖️ 多起点 <br>

一个流程图中可以设置多个发起节点，自动匹配相应节点，简化配置，维护高效

<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/multi_start.png" width=500 ><br>

🎖️ 一站式流程调试 <br>

流程进入调试模式后，可以用一个用户登录，走完全流程，无需用流程中参与的每个用户重新登录
<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/debug_deliver.png" width=800 ><br>

在调试面板上，可以配置可填写字段、隐藏字段，运行事件脚本
<img src="https://gitee.com/bestfeng/oa_git_free/raw/master/ad-img/debug_panel.png" width=800 ><br>


### 系统优势

| 功能     | 描述                                                 |
|--------|----------------------------------------------------|
| 专业流程引擎 | 1、支持多起点，多个起点自动适配发起人，省去大量的条件判断，极大降低流程复杂度<br/>2、跳签，没有用户的情况一可以跳过节点，支持复杂组织机构的利器<br/>3、选择策略，支持竞争、最闲者优先、角色中X人处理等策略<br/>4、超时策略，超时可等待、自动交办至下一节点或返回给发起人<br/>5、异步提交，节点上有多个人员同时处理时，可以分别提交，适用于任务下达的情况<br/>6、矩阵式组织架构，支持项目团队人员分组，与原有组织架构一起协同流转<br/>7、一站式调试，支持一个用户登录测完全程，无需切换帐户反复登录，在测试过程中设置可写字段、隐藏字段以及运行事件脚本，实施起来方便快捷 |
| 表单<br/>设计     | 1、支持选择记录带入相关字段，如选择项目，带入项目地址、完成时间、可用预算等<br/>2、支持拉单，可选择多条记录一起拉取，并可以自动拉单，如：选择人员，自动拉取其借款记录<br/>3、自动冲抵，如：报销流程走完后，自动冲抵所拉取的借款记录，将来便不会再被拉取到<br/>4、显示规则，支持根据条件判断显示或隐藏区域<br/>5、验证规则，支持根据条件判断进行表单数据有效性校验<br/>6、自带61个宏控件，通过宏控件标准扩展接口可实现无限扩展                   |
| 事件<br/>开发   | 1、支持在线java语法检查、自动提示，代码即刻生效，无需浪费时间编译上传重启<br/>2、流程中支持验证、流转、退回、结束、放弃、撤回、删除等事件<br/>3、模块中支持验证、添加、修改、删除、导入前验证、导入后清洗数据等事件                   |

### 技术门槛

- 流程表单配置零门槛，普通办公室文员也可以快速学会。  <br>
- 二次开发，只需一年的java经验，就可以玩转。<br>

### 学习QQ群

<img alt="群二维码" src="https://images.gitee.com/uploads/images/2021/1012/140640_5b66816c_48408.png" width="130" align="center"/>  
<img alt="群二维码" src="https://images.gitee.com/uploads/images/2021/1012/140822_085b8135_48408.png" width="130" align="center"/>  
<img alt="群二维码" src="https://images.gitee.com/uploads/images/2021/1012/140850_6e1b9977_48408.png" width="130" align="center"/>  
<img alt="群二维码" src="https://gitee.com/bestfeng/yimioa/raw/oa9.0/ad-img/xc1.png" width="130" align="center"/>  
<img alt="群二维码" src="https://gitee.com/bestfeng/yimioa/raw/oa9.0/ad-img/xc2.png" width="130" align="center"/>  
<br>
<br>

- 立即扫描上面 👉最右侧 的二维码入群
- 进群后，即刻免费获得相关文档及数据字典！<br>
- 进群后，即可按推送链接进入演示地址在线体验<br>
- 进群后，立享1小时免费培训，助您成为流程专家<br>
🚀 千人大群里小伙伴正在快速增长中！	<br>

### 友情提醒

- 如果您觉得工作流不太好掌握，建议立即进群，“行云”流程引擎会让您感觉所谓的复杂也不过如此。<br>
- 也许您找到的正是一款将属于你自己的宝藏级的流程引擎！<br>

